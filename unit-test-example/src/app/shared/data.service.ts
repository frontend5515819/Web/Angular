import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  dataResolved = false;

  async getDetails(): Promise<string> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.dataResolved = true;
        resolve('Data');
      }, 1500);
    });
  }
}
