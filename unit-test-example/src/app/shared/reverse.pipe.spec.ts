import { ReversePipe } from './reverse.pipe';

describe('pipe tests', () => {
  it('should correctly reverse word', () => {
    let reversePipe = new ReversePipe();
    expect(reversePipe.transform('hello')).toEqual('olleh');
  });
});
