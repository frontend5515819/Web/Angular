import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ServerComponent } from './server/server.component';
import { ServersComponent } from './servers/servers.component';
import { WarningAlertComponent } from './warning-alert/warning-alert.component';
import { SuccessAlertComponent } from './success-alert/success-alert.component';
import { UsernameInputComponent } from './username-input/username-input.component';
import { DetailsDisplayComponent } from './details-display/details-display.component';

// A modules is nothing else than a class with an NGModule decoractor 
@NgModule({
  declarations: [ // this list all the compoenents that can be used in the AppModule
    AppComponent,
    ServerComponent,
    ServersComponent,
    WarningAlertComponent,
    SuccessAlertComponent,
    UsernameInputComponent,
    DetailsDisplayComponent
  ],
  imports: [ // allows to add other modules
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
