import { Component } from '@angular/core';

@Component({
  selector: 'app-details-display',
  templateUrl: './details-display.component.html',
  styleUrl: './details-display.component.css'
})
export class DetailsDisplayComponent {

  isDetailsVisible = false; 
  buttonLogs : Date[] = []; 

  onDisplayButtonClicked(){
    this.isDetailsVisible = !this.isDetailsVisible; 
  
    this.buttonLogs.push(new Date(Date.now())); 
  }

  isRecent(index: number){
    return index > 3; 
  }

  getBackgroundColor(index: number){
    if (this.isRecent(index)){
      return "#4287f5";
    }

    return "inherit"; 
  }
}
