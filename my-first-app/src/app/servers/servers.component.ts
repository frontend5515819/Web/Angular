import { Component } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  //template: '<app-server></app-server><app-server></app-server>', // template allows defining the element in the ts file
  styleUrl: './servers.component.css'
})
export class ServersComponent {
  
  serverCreationStatus = "No server was created."
  serverName = '';
  serverWasCreated = false; 

  servers = ["Test Server 1", "Test Server 2"]; 

  constructor(){
  }

  onCreateServer(){
    this.serverWasCreated = true; 
    this.servers.push(this.serverName);
    this.serverCreationStatus = `Server was created with name ${this.serverName}`; 
  }

  onUpdateServerName(event: InputEvent){
    this.serverName = (<HTMLInputElement>event.target).value; 
  }

  canAddNewServer(){
    return this.serverName !== ""; 
  }
}
