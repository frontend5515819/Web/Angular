// a component in this case is nothing more than a class 
// the class needs to be exported, so that angular knows where to search for it

import { Component } from '@angular/core'; 

// to make the class recognizable as a compoenent, the component decorator needs to be added 
@Component({ // the component needs to be configured by a javascript object
    selector: 'app-server', // selector sets the tag of the component and should be unique
    templateUrl: './server.component.html', // the html file is given here relative to the typescript file
    styleUrl: './server.component.css'
})
export class ServerComponent {
    serverId: number = 10; 
    serverStatus: ServerStatus = ServerStatus.Offline; 

    constructor(){
        this.serverStatus = Math.random() > 0.5 ? ServerStatus.Online : ServerStatus.Offline; 
    }

    getStatusColor(){
        return this.serverStatus === ServerStatus.Online ? '#62e36a' : '#de3a4a';
    }

    isServerOnline(){
        return this.serverStatus === ServerStatus.Online; 
    }
}

enum ServerStatus{
    Online = "online", 
    Offline = "offline", 
}