import { Component } from '@angular/core';

@Component({
  selector: 'app-username-input',
  templateUrl: './username-input.component.html',
  styleUrl: './username-input.component.css'
})
export class UsernameInputComponent {

  username: string = ''; 

  canResetUserName(){
    return this.username != ''; 
  }

  onUserNameReset(){
    this.username = ''; 
  }

}
