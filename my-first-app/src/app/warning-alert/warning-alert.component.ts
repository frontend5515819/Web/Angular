import { Component } from '@angular/core';

@Component({
  selector: 'app-warning-alert',
  template: '<p>Warning</p>',
  styles: `
  p {
    display: inline-block;
    background-color: rgb(245, 84, 66);
    padding: 1rem; 
    border-radius: 1rem;
    border: solid 1px rgb(171, 51, 38);
    color: rgb(64, 49, 47);
  }
  `
})
export class WarningAlertComponent {

}
