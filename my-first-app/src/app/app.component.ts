import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  //styleUrl: './app.component.css' - it is possible to have multiple css files listed here 
  styles: [`
    h3 {
      color: dodgerblue;
    }

    .container{
      margin-top: 1rem;
    }
  `]
})
export class AppComponent {
}
