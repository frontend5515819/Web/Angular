# 1. How Angular is loaded 

## 1.1 Loading the index.html 

- Angular is a single page application
- The single page is actually the index.html file 
- Within the index.html file the `<app-root>` tag is compoenent that will be injected into the website during runtime 
- At the end of the index.html, angular will inject bundles of javascript code to make the website run 
- Those bundles are created during build and cannot be seen in the index.html

## 1.2 Loading the module 

- The bundles will contain the actual programming code 
- The first entry point of those scripts is the main.ts file 
- Here the main.ts bootstraps the AppModule 
- Within the AppModule which is defined in the app.module.ts, it exports the AppComponent 
- The AppComponent is defined in the app.component.ts
- There the decorator defines the selector, so that Angular knows what the app-root tag means

# 2. Components 

Components are reusable elements in Angular. 

Components have their own html template, styling and business logic. 

The benefit of having components is to used them repeatedly in different places and splitting complexity of the application into smaller parts. 

## 2.1 Components - General rule 

Each component should have its own folder. 

The folder should be named after the component.

## 2.2 Creating Components from the CLI 

Components can be created quickly by using the following command: 

```ps
    ng generate component [component_name]
```

This will create automatically a new component contained in a new folder. 

# 3. Modules

Modules allow to bundle the components and define which component can access which other component. 

# 4. Databinding 



## 4.1 String Interpolation

String interpolation allows to bind to properties or methods from the class to the html template by converting the values into strings and then displaying them in the UI. 

String interpolation is used with two brackets: 

```html
<p>My name is {{ name }}</p>
```

## 4.2 Property Binding

Property Binding allows to assign values from the class or code behind to the attribute values from html elements. This allows e.g. to disable or enable buttons. The syntax for property binding is to use square brackets.

```html 
<button [disabled]="!isButtonEnabled">
```

## 4.3 Property Binding vs. String Interpolation 

Property Binding can in fact replace the functionality of string interpolation, by setting the "innerText" property of an html tag like this: 

```html
<p [innerText]="name"></p>
<p>{{ name }}</p> 
```

It is still recommended to use string interpolation if text is involved or the inner text has to be modified. Otherwise, property binding is mostly used.



