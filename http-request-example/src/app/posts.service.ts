import { Injectable } from '@angular/core';
import { Post } from './post.model';
import {
  HttpClient,
  HttpEventType,
  HttpHeaders,
  HttpParams,
} from '@angular/common/http';
import { Subject, catchError, map, tap, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PostsService {
  isFetching = false;
  error = new Subject<string>();

  constructor(private http: HttpClient) {}

  createPosts(title: string, content: string) {
    const post = { title: title, content: content };

    this.http
      .post<{ name: string }>(
        'https://angular-http-example-9d49c-default-rtdb.europe-west1.firebasedatabase.app/posts.json',
        post,
        {
          observe: 'response',
        }
      )
      .subscribe(
        (responseData) => {
          console.log(responseData);
        },
        (error) => {
          this.error.next(error.message);
        }
      );
  }

  fetchPosts() {
    let searchParams = new HttpParams();
    searchParams = searchParams.append('print', 'pretty');
    searchParams = searchParams.append('custom', 'key');

    return this.http
      .get<{ [key: string]: Post }>(
        'https://angular-http-example-9d49c-default-rtdb.europe-west1.firebasedatabase.app/posts.json',
        {
          headers: new HttpHeaders({
            'Custom-Header': 'hello',
          }),
          params: searchParams,
        }
      )
      .pipe(
        map((responseData) => {
          const postsArray: Post[] = [];

          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {
              postsArray.push({
                ...responseData[key],
                id: key,
              });
            }
          }

          return postsArray;
        }),
        catchError((errorRes) => throwError(() => errorRes))
      );
  }

  deletePosts() {
    return this.http
      .delete(
        'https://angular-http-example-9d49c-default-rtdb.europe-west1.firebasedatabase.app/posts.json',
        {
          observe: 'events',
        }
      )
      .pipe(
        tap((event) => {
          console.log(event);
          if (event.type === HttpEventType.Sent) {
            console.log('Request sent.');
          }
          if (event.type === HttpEventType.Response) {
            console.log(event.body); // would be the response
          }
        })
      );
  }
}
