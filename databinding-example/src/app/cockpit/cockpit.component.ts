import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrl: './cockpit.component.css'
})
export class CockpitComponent implements OnInit{

  @Output()
  serverCreated = new EventEmitter<{name: string, content: string}>(); 
  
  @Output()
  blueprintCreated = new EventEmitter<{name: string, content: string}>();

  // newServerName = '';
  newServerContent = '';

  // instead of using the local reference, the component type can also be used 
  @ViewChild('serverContentInput', {static: true})
  serverContentInput: ElementRef; 

  ngOnInit(): void {
  }

  onAddServer(nameInput: HTMLInputElement) {
    this.serverCreated.emit({
      name: nameInput.value,
      content: this.serverContentInput.nativeElement.value,
    });
  }

  onAddBlueprint(nameInput: HTMLInputElement) {
    this.blueprintCreated.emit({
      name: nameInput.value,
      content: this.serverContentInput.nativeElement.value,
    })
  }

}
