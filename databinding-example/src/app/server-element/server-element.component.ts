import { AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Component, ContentChild, DoCheck, ElementRef, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrl: './server-element.component.css',
  encapsulation: ViewEncapsulation.None
})
export class ServerElementComponent implements 
  OnInit, 
  OnChanges, 
  DoCheck, 
  AfterContentInit, 
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
  OnDestroy {
  
  // the element property needs to be explicitly bindable or exposed to other components 
  // the alias is used from the outside 
  @Input('serverElement')
  element: {
    type: string, 
    name: string, 
    content: string, 
  };

  @ViewChild("heading", {static: true})
  heading: ElementRef;

  // access elements from ng-content (is actually passed from the parent object)
  @ContentChild("contentParagraph", {static: true})
  paragraph: ElementRef;

  constructor(){
    console.log("constructor called"); 
  }

  ngOnDestroy(): void {
    console.log("ngOnDestroy called.");
  }

  ngAfterViewChecked(): void {
    console.log("ngViewChecked called.");
  }

  ngAfterViewInit(): void {
    console.log("ngViewInit called."); 
    console.log("Text Content:" + this.heading.nativeElement.textContent); // available
  }

  ngAfterContentChecked(): void {
    console.log("ngAfterContentChecked called.");
  }

  ngAfterContentInit(): void {
    // only fire once when the content is injected
    console.log("ngAfterContentInit called!.");
  }

  ngDoCheck(): void {
    // triggers -> events or resolved promises 
    console.log("ngDoCheck called"); 
  }

  ngOnChanges(changes: SimpleChanges){
    console.log("ngOnChanges called."); 
    console.log(changes);
  }

  ngOnInit(): void {
    console.log("ngOnInit called!");
    console.log("Text Content:" + this.heading.nativeElement.textContent); // not available yet
  }
}
