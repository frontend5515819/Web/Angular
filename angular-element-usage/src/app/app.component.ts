import { Component } from '@angular/core';
import { CustomTextComponent } from '../../../angular-element-example/src/app/custom-text/custom-text.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CustomTextComponent],
  template: `<app-custom-text></app-custom-text>`,
})
export class AppComponent {
  title = 'angular-element-usage';
}
