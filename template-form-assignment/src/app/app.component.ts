import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  title = 'Template-Based Forms Assignment';

  readonly subscriptions = ['Basic', 'Advanced', 'Pro'];
  defaultSubscription = 'Advanced';

  submitted = false;

  subscriptionData = {
    email: '',
    password: '',
    subscription: '',
  };

  @ViewChild('subscriptionForm')
  subscriptionForm!: NgForm;

  onSubmit() {
    this.subscriptionData.email = this.subscriptionForm.value.email;
    this.subscriptionData.password = this.subscriptionForm.value.password;
    this.subscriptionData.subscription =
      this.subscriptionForm.value.subscription;

    this.submitted = true;
  }

  onBackButtonClicked() {
    this.submitted = false;
  }
}
