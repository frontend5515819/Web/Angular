import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrl: './game-control.component.css'
})
export class GameControlComponent {

  private interval: any; 
  private lastNumber: number = 0; 

  isGameRunning = false; 

  @Output()
  intervalFired = new EventEmitter<number>(); 
  
  onStartButtonClicked(){
    
    this.interval = setInterval(() => this.intervalFired.emit(++this.lastNumber), 1000);

    this.isGameRunning = true; 
  
  }

  onPauseButtonClicked(){

    clearInterval(this.interval);

    this.isGameRunning = false; 

  }
}
