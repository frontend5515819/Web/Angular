import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {

  title = 'databinding-assigment';

  numbers: number[] = []; 

  private timeoutHandler: NodeJS.Timeout | undefined;

  onIntervalFired(newNumber: number){
    this.numbers.push(newNumber); 
  }
}
