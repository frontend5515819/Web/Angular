import { Component, OnInit } from '@angular/core';
import { UserHistoryService } from '../../../services/user-history.service';

@Component({
  selector: 'app-user-history',
  templateUrl: './user-history.component.html',
  styleUrl: './user-history.component.css'
})
export class UserHistoryComponent implements OnInit {

  historyEntries: {name: string, oldStatus: string, newStatus: string}[] = [];

  constructor(private userHistoryService: UserHistoryService){}

  ngOnInit(): void {
    this.historyEntries = this.userHistoryService.historyEntries;
  }

}
