import { Injectable } from "@angular/core";

@Injectable({providedIn: "root"})
export class UserHistoryService {

    historyEntries: {name: string, oldStatus: string, newStatus: string}[] = []; 

    addHistoryEntry(user: string, oldStatus: string, newStatus: string){
        this.historyEntries.push({name: user, oldStatus, newStatus});
    }

} 