import { Injectable } from "@angular/core";
import { UserHistoryService } from "./user-history.service";

@Injectable({providedIn: "root"})
export class UserService {
    
    inactiveUsers: string[] = ['Chris', 'Manu'];
    activeUsers: string[] = ['Max', 'Anna'];

    constructor(private userHistoryService: UserHistoryService){}

    moveToActive(id: number){
        let user = this.inactiveUsers[id]; 

        this.activeUsers.push(this.inactiveUsers[id]); 
        this.inactiveUsers.splice(id, 1);

        this.userHistoryService.addHistoryEntry(user, "inactive", "active"); 
    }

    moveToInactive(id: number){
        let user = this.activeUsers[id];

        this.inactiveUsers.push(this.activeUsers[id]); 
        this.activeUsers.splice(id, 1); 

        this.userHistoryService.addHistoryEntry(user, "active", "inactive"); 
    }
}