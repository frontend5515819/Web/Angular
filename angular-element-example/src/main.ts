import { createCustomElement } from '@angular/elements';
import { createApplication } from '@angular/platform-browser';
import { CustomTextComponent } from './app/custom-text/custom-text.component';

// bootstrapApplication(AppComponent, appConfig)
//   .catch((err) => console.error(err));

(async () => {
  const app = await createApplication({
    providers: [],
  });

  const customTextElement = createCustomElement(CustomTextComponent, {
    injector: app.injector,
  });

  customElements.define('app-custom-text', customTextElement);
})();
