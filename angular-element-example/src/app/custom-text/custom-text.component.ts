import { Component } from '@angular/core';

@Component({
  selector: 'app-custom-text',
  standalone: true,
  imports: [],
  template: ` <p style="color: red;">custom-text works!</p> `,
  styles: ``,
})
export class CustomTextComponent {}
