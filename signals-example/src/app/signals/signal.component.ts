import { NgFor } from '@angular/common';
import {
  Component,
  OnInit,
  WritableSignal,
  computed,
  effect,
  signal,
} from '@angular/core';

@Component({
  selector: 'app-signal',
  templateUrl: './signal.component.html',
  standalone: true,
  imports: [NgFor],
})
export class SignalComponent implements OnInit {
  actions = signal<string[]>([]);
  counter = signal(0);
  doubleCounter = computed(() => this.counter() * 2);

  ngOnInit(): void {
    effect(() => console.log(this.counter()));
  }

  increment() {
    this.counter.update((counter) => counter + 1);
    this.actions.update((actions) => [...actions, 'INCREMENT']);

    console.log(this.counter());
  }

  decrement() {
    this.counter.update((counter) => counter - 1);
    this.actions.update((actions) => [...actions, 'DECREMENT']);
  }
}
