import { Directive, ElementRef, HostBinding, HostListener, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit{

  @Input()
  defaultColor: string = "transparent"; 

  @Input()
  highlightColor: string = "blue"; 

  // defines the property to which to bind to 
  @HostBinding("style.backgroundColor")
  backgroundColor: string; 

  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }
  
  ngOnInit(): void {
    // using the renderer instead of changing the style directly on the element is the prefferred way 
    // this ensures the connection and access to the DOM 
    this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', this.defaultColor);
  }

  @HostListener("mouseenter")
  mouseOver(eventData: Event) {
    //this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', 'blue');
    this.backgroundColor = this.highlightColor; 
  }

  @HostListener("mouseleave")
  mouseLeave(eventData: Event) {
    //this.renderer.setStyle(this.elementRef.nativeElement, "background-color", "transparent"); 
    this.backgroundColor = this.defaultColor;
  }

}
