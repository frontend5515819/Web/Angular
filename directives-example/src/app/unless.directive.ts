import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {

  @Input()
  set appUnless(condition: boolean){
    if(!condition){
      this.viewContainerRef.createEmbeddedView(this.templateRef); 
    }
    else {
      this.viewContainerRef.clear(); 
    }
  }

  // Since the structural directive is transformed into ng-template
  // first the template reference is needed 
  // second the viewcontainer reference is needed (place where the template should be placed)
  constructor(private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) 
  {

  }

}
