import { Component, OnInit, ViewChild } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  NgForm,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent implements OnInit {
  title = 'Reactive Forms Assignment';

  readonly projectStates = ['Stable', 'Critical', 'Finished'];

  submitted = false;

  projectData = {
    projectname: '',
    email: '',
    projectstate: '',
  };

  projectForm!: FormGroup;

  ngOnInit() {
    this.projectForm = new FormGroup({
      projectname: new FormControl(
        null,
        [Validators.required],
        this.isProjectNameAllowed.bind(this)
      ),
      email: new FormControl(null, [Validators.required, Validators.email]),
      projectstate: new FormControl(this.projectStates[0]),
    });
  }

  onSubmit() {
    this.projectData = {
      projectname: this.projectForm?.get('projectname')?.value ?? '',
      email: this.projectForm?.get('email')?.value ?? '',
      projectstate: this.projectForm?.get('projectstate')?.value ?? '',
    };

    this.submitted = true;
  }

  onBackButtonClicked() {
    this.submitted = false;
    this.projectForm.reset();
  }

  isEmailInvalid() {
    return (
      this.projectForm.get('email')?.touched &&
      !this.projectForm.get('email')?.valid
    );
  }

  isProjectNameAllowed(
    control: AbstractControl<any, any>
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'Test') {
          resolve({ projectnameallowed: false });
        } else {
          resolve(null);
        }
      });
    });
  }
}
