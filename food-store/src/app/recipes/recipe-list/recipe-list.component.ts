import { Component, OnDestroy, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrl: './recipe-list.component.css',
})
export class RecipeListComponent implements OnInit, OnDestroy {
  recipes: Recipe[];
  recipeChangedSubscription: Subscription;

  constructor(
    private recipeService: RecipeService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.recipes = this.recipeService.getRecipes();

    this.recipeChangedSubscription =
      this.recipeService.recipesChanged.subscribe((newRecipes) => {
        this.recipes = newRecipes;
      });
  }

  ngOnDestroy(): void {
    this.recipeChangedSubscription.unsubscribe();
  }

  onNewRecipeButtonClicked() {
    this.router.navigate(['new'], { relativeTo: this.route });
  }
}
