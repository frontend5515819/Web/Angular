import { Directive, ElementRef, HostBinding, HostListener, Renderer2 } from "@angular/core";

@Directive({
    selector: '[appDropdown]' 
})
export class DropdownDirective{

    @HostBinding('class.open')
    isOpen = false; 

    @HostListener('document:click', ['$event'])
    toggleOpen(eventData: Event){
    
        // clicked inside the button
        if (this.elementRef.nativeElement.contains(eventData.target)){
            this.isOpen = !this.isOpen;
        }
        // clicked anywhere outside 
        else {
            this.isOpen = false;
        }
    
    }

    constructor(private elementRef: ElementRef){

    }

}