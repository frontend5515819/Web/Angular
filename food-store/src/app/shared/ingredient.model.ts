export class Ingredient {
  // This will automatically create the properties
  constructor(public name: string, public amount: number) {}
}
