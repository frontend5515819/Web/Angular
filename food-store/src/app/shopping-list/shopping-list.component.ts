import { Component, OnDestroy, OnInit } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from './shopping-list.service';
import { Subscription } from 'rxjs';
import { LoggingService } from '../logging.module';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrl: './shopping-list.component.css',
})
export class ShoppingListComponent implements OnInit, OnDestroy {
  private ingredientsChangeSubscription: Subscription;

  ingredients: Ingredient[] = [];

  constructor(
    private shoppingListService: ShoppingListService,
    private loggingService: LoggingService
  ) {}

  ngOnInit(): void {
    this.ingredients = this.shoppingListService.getIngredients();
    this.ingredientsChangeSubscription =
      this.shoppingListService.ingredientChanged.subscribe(
        () => (this.ingredients = this.shoppingListService.getIngredients())
      );
    this.loggingService.printLog('Hello from ShoppingListComponent ngOnInit');
  }

  ngOnDestroy(): void {
    this.ingredientsChangeSubscription.unsubscribe();
  }

  onItemEdit(index: number) {
    this.shoppingListService.startedEditing.next(index);
  }
}
