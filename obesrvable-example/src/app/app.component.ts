import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from './user/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  private userActivatedSubscription: Subscription;

  userActivated = false;

  constructor(private userService: UserService) {}

  ngOnDestroy(): void {
    this.userActivatedSubscription.unsubscribe();
  }

  ngOnInit() {
    this.userActivatedSubscription =
      this.userService.activatedEmitter.subscribe(
        (active) => (this.userActivated = active)
      );
  }
}
