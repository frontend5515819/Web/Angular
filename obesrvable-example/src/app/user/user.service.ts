import { EventEmitter, Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UserService {
  // Subject preffered to event emitter
  activatedEmitter = new Subject<boolean>();
}
