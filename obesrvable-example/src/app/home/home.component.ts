import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, interval, Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
  private intervalSubscription: Subscription;

  constructor() {}

  ngOnInit() {
    // this.intervalSubscription = interval(1000).subscribe((count) => {
    //   console.log(count);
    // });
    const customIntervalObservable = new Observable((observer) => {
      let count = 0;
      setInterval(() => {
        observer.next(count);

        if (count == 2) {
          observer.complete();
        }

        // throwing an error will kill all subscribtions
        if (count > 3) {
          observer.error(new Error('Count is divisable by 5!'));
        }

        count++;
      }, 1000);
    });

    let extendedCustomInterval = customIntervalObservable.pipe(
      filter((data: number) => {
        return data % 2 == 0;
      }),
      map((data: number) => {
        return `Round: ${data}`;
      })
    );

    this.intervalSubscription = extendedCustomInterval.subscribe(
      (count) => console.log(count),
      (error) => console.log(error),
      () => console.log('completed!')
    );
  }

  ngOnDestroy(): void {
    this.intervalSubscription.unsubscribe();
  }
}
